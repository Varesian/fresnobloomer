#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "Bloomer.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class BloomerApp : public AppNative {
  public:
	void setup();
	void mouseDown( MouseEvent event );	
	void update();
	void draw();
	void setDeltaTime();
	float deltaTime, lastRecordedTime;
	Bloomer bloomer;
};

void BloomerApp::setup()
{
	deltaTime = lastRecordedTime = 0;
	bloomer = Bloomer(Vec2f(0, 0));
}

void BloomerApp::mouseDown( MouseEvent event )
{
}

void BloomerApp::update()
{
	setDeltaTime();
	bloomer.update(deltaTime);
}

void BloomerApp::draw()
{
	// clear out the window with black
	gl::clear( Color( 0, 0, 0 ) ); 

	bloomer.draw();
}

void BloomerApp::setDeltaTime() {

	float curRecordedTime = cinder::app::getElapsedSeconds();
	deltaTime =  curRecordedTime - lastRecordedTime;
	lastRecordedTime = curRecordedTime;
}

CINDER_APP_NATIVE( BloomerApp, RendererGl )
